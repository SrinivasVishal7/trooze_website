import React from 'react';
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import TopNavBar from './Navigation/TopNavBar';
import './App.css';

function App() {
  return (
    <div className="App">
      <TopNavBar/>
    </div>
  );
}

export default App;
